var express = require('express');
var router = express.Router();
var Cart = require('../models/cart');
var User = require('../models/user');
var Product = require('../models/product');
var Order = require('../models/order');
var tablaUsers = require('../models/users');
var rest = require('../mymodules/rest');


router.get('/', function(req, res, next) {
  var successMsg = req.flash('success')[0];
  Product.find(function(err, docs) {
    var productChunks = [];
    var chunkSize = 3;
    for (var i = 0; i < docs.length; i += chunkSize) {
      productChunks.push(docs.slice(i, i + chunkSize));
    }
    res.render('shop/index', {
      title: 'Carrito de compras',
      products: productChunks,
      successMsg: successMsg,
      noMessages: !successMsg
    });
  });
});

router.get('/add-to-cart/:id', function(req, res, next) {
  var productId = req.params.id;
  var cart = new Cart(req.session.cart ? req.session.cart : {});

  Product.findById(productId, function(err, product) {
    if (err) {
      return res.redirect('/');
    }
    cart.add(product, product.id);
    req.session.cart = cart;
    res.redirect('/');
  });
});

router.get('/reduce/:id', function(req, res, next) {
  var productId = req.params.id;
  var cart = new Cart(req.session.cart ? req.session.cart : {});

  cart.reduceByOne(productId);
  req.session.cart = cart;
  res.redirect('/shopping-cart');
});

router.get('/remove/:id', function(req, res, next) {
  var productId = req.params.id;
  var cart = new Cart(req.session.cart ? req.session.cart : {});

  cart.removeItem(productId);
  req.session.cart = cart;
  res.redirect('/shopping-cart');
});

router.get('/shopping-cart', function(req, res, next) {
  if (!req.session.cart) {
    return res.render('shop/shopping-cart', {
      products: null
    });
  }
  var cart = new Cart(req.session.cart);
  res.render('shop/shopping-cart', {
    products: cart.generateArray(),
    totalPrice: cart.totalPrice
  });
});


router.post('/loginFB',function(req,res,next){
  var redirect_uri = "http://localhost:3000/facebook_redirect"
  res.redirect("https://www.facebook.com/v2.10/dialog/oauth?client_id=1136026253195221&redirect_uri="+redirect_uri);
});

router.get('/facebook_redirect',function(req,res,next){
  //capturar datos de facebook
  	var redirect_uri = "http://localhost:3000/facebook_redirect";
  	var facebook_code = req.query.code;
  	var accesstoken_call = {
  	    host: 'graph.facebook.com',
  	    port: 443,
  	    path: '/v2.10/oauth/access_token?client_id=1136026253195221&redirect_uri='+redirect_uri+
  	    '&client_secret=926add611effd76906a1fb2375ebc75e&code='+facebook_code,
  	    method: 'GET',
  	    headers: {
  	        'Content-Type': 'application/json'
  	    }
  	};

  	rest.getJSON(accesstoken_call, function(statusCode, access_token_response) {
  	    var FB_path = '/me?fields=id,name,email,picture&access_token='+access_token_response.access_token;
  	    console.log("token: " + access_token_response.access_token);
  	    console.log("FB_path: " + FB_path);
  	    var userinfo_call = {
  		    host: 'graph.facebook.com',
  		    port: 443,
  		    path: FB_path,
  		    method: 'GET',
  		    headers: {
  		        'Content-Type': 'application/json'
  		    }
  		};
  		rest.getJSON(userinfo_call, function(statusCode, user_info_response) {
  	    	console.log("USUARIO ID :::::::::::::::: " + user_info_response.name);
  			tablaUsers.find( {where: { name : user_info_response.name  }} ).then(function(user){ //facebookID en tabla de usuarios y facebook access token
  				//user.facebook_access_token = access_token_response.access_token;

     var newUser = {
         "email":user_info_response.email ,
         "name":user_info_response.name ,
         "username":"",
         "password":access_token_response.access_token,
         "address":"",
         "status":"active"
     };

     tablaUsers.build(newUser).save().then(function(){
       console.log("LISTO");
       res.json(newUser);
        });

  	});
  });
  });
});

  router.get('/createuser', function(req, res, next) {
    var query = {"email":req.body.email,"name":req.body.name, "username":req.body.username, "password":req.body.password,"address":req.body.address};
    //User.save(query);
    res.json(query);
  });

module.exports = router;

function isLoggedIn(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }
  req.session.oldUrl = req.url;
  res.redirect('/user/signin');
}
