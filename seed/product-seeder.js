var Product = require('../models/product');

var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/store2');
mongoose.Promise = global.Promise;

var products = [
  new Product({
    imagePath: 'https://images-submarino.b2w.io/produtos/01/00/sku/10633/1/10633182_1GG.jpg',
    title: 'Pes 2016 - Ps4',
    description: 'Pro Evolution Soccer 2016 (oficialmente abreviado como PES 2016, y llamado World Soccer: Winning Eleven 2016 en Japón) es un videojuego de fútbol de la serie Pro Evolution Soccer desarrollado y publicado por Konami. El juego fue anunciado el 12 de junio de 2015, salió a la venta el 15 de septiembre de 2015 en América y el 17 de septiembre del mismo año en el resto del Mundo.',
    price: 1099
  }),
  new Product({
    imagePath: 'https://images-submarino.b2w.io/produtos/01/00/item/21014/3/21014382_1SZ.jpg',
    title: 'Tom Clancys Ghost Recon Wildlands - Ps4',
    description: 'Tom Clancys Ghost Recon Wildlands es un videojuego de disparos táctico de mundo abierto en desarrollo por Ubisoft Paris y distribuido por Ubisoft para Microsoft Windows, PlayStation 4 y Xbox One. Será la décima entrega de la franquicia de Tom Clancys Ghost Recon y el primer juego en incluir un ambiente de mundo abierto. El videojuego dejara atrás el escenario futurista presentado en Tom Clancys Ghost Recon Advanced Warfighter e incluirá un escenario similar al que tenía el original. Su fecha de lanzamiento esta prevista para el 7 de marzo de 2017.',
    price: 999
  }),
  new Product({
    imagePath: 'https://images-submarino.b2w.io/produtos/01/00/item/127373/6/127373656SZ.jpg',
    title: 'Lego Star Wars: El despertar de la fuerza  - PS4',
    description: 'LEGO Star Wars: El Despertar de la Fuerza es un videojuego de acción-aventura Lego desarrollado por TT Games y publicado por Warner Bros. Interactive Entertainment. Es la quinta entrega de la serie de videojuegos de Lego Star Wars, que salió a la venta el 28 de junio de 2016, para Microsoft Windows, PlayStation 3, PlayStation 4, Xbox 360, Xbox One, Nintendo 3DS, PlayStation Vita y Wii U.',
    price: 799
  }),
  new Product({
    imagePath: 'https://images-submarino.b2w.io/produtos/01/00/item/122163/8/122163887SZ.jpg',
    title: 'Mortal Kombat X - PS4',
    description: 'Mortal Kombat X (abreviado MKX) es un videojuego de pelea creado por Ed Boon, desarrollado por NetherRealm Studios y publicado por Warner Bros. Interactive Entertainment, fue anunciado en junio de 2014, mediante un vídeo que mostraba a Sub-Zero y Scorpion peleando entre sí. Está disponible para PlayStation 4, Xbox One y PC desde el 14 de abril de 2015; para iOS desde el 7 de abril de 2015 y para el Android desde el 21 de abril de 2015. El eslogan oficial es: "Whos next?" (lit. ¿Quién sigue?).',
    price: 1199
  }),
  new Product({
    imagePath: 'https://images-submarino.b2w.io/produtos/01/00/item/124776/7/124776759_1GG.jpg',
    title: "Uncharted 4: A Thief's End - PS4",
    description: "Uncharted 4: A Thief's End (Uncharted 4: El desenlace del ladrón en España, Uncharted 4: El Fin de un Ladrón en Hispanoamérica), es un videojuego de acción-aventura en tercera persona, lanzado el 10 de mayo de 2016, distribuido por Sony Computer Entertainment y desarrollado Naughty Dog exclusivamente para PlayStation 4. Es la secuela de Uncharted 3: La traición de Drake, la cuarta de la serie Uncharted. El videojuego fue confirmado con su primer teaser tráiler en noviembre del 2013 y luego fue mostrado en el E3 2014.",
    price: 1199
  }),
  new Product({
    imagePath: 'https://images-submarino.b2w.io/produtos/01/00/item/126036/2/126036287SZ.jpg',
    title: "The Witcher 3: Wild Hunt - PS4",
    description: "The Witcher 3: Wild Hunt (polaco: Wiedźmin 3: Dziki Gon) es un videojuego de rol desarrollado por CD Projekt RED. CD Projekt RED es el desarrollador mientras que la distribución corre a cargo de la Warner Bros. Interactive, en el caso de Norteamérica y Bandai Namco para Europa. Fue anunciado en febrero de 2013 y su lanzamiento tuvo lugar, a nivel mundial, el 19 de mayo de 2015 para PlayStation 4, Xbox One y Microsoft Windows. The Witcher 3: Wild Hunt retrasa su fecha de lanzamiento al 19 de mayo de 2015.",
    price: 999
  })
];

var done = 0;
for (var i = 0; i < products.length; i++) {
  products[i].save(function(err, result) {
    done++;
    if (done === products.length) {
      exit();
    }
  });
}

function exit() {
  mongoose.disconnect();
}
